function addPerson() {
	var theDialog = $('#personDialog').dialog("option", "title", 'Add Book');
	theDialog.dialog('open');
}

function editPerson(id) {

	$.get("get/" + id, function(result) {

		var theDialog=$("#personDialog").html(result);

		theDialog.dialog("option", "title", 'Edit Book');

		theDialog.dialog('open');
	});
}


function resetDialog(form) {

	form.find("input").val("");
}

$(document).ready(function() {

	$('#personDialog').dialog({

		autoOpen : false,
		position : 'center',
		modal : true,
		resizable : false,
		width : 440,
		buttons : {
			"Save" : function() {
				$('#personForm').submit();
			},
			"Cancel" : function() {
				$(this).dialog('close');
			}
		},
		close : function() {

			resetDialog($('#personForm'));

			$(this).dialog('close');
		}
	});
});
