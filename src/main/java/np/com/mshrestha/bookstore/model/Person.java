package np.com.mshrestha.bookstore.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class Person {

	private Integer id;
	private String name;
	private Integer age;
	private Boolean isAdmin;
	private Timestamp createdDate;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(length = 8)
	public Integer getId() {
		return id;
	}

	@Column(nullable = false,length = 25)
	public String getName() {
		return name;
	}

	@Column(nullable = false)
	public Integer getAge() {
		return age;
	}

	@Column(nullable = false)
	public Boolean getIsAdmin() {
		return isAdmin;
	}

	@Column(nullable = false)
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
}
