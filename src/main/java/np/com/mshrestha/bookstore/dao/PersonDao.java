package np.com.mshrestha.bookstore.dao;

import java.util.List;

import np.com.mshrestha.bookstore.model.Person;

public interface PersonDao {

    /*
     * CREATE and UPDATE
     */
    public void savePerson(Person person); // create and update

    /*
     * READ
     */
    public List<Person> listPersons();

    public Person getPerson(Integer id);

    /*
     *  Search
     */
    public List<Person> getPersonByName(String name);

    /*
     * DELETE
     */
    public void deletePerson(Integer id);

    public List<Person> viewAllPersons(int offset,int noOfRecords);

    public int tableSize();
}
