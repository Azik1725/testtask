package np.com.mshrestha.bookstore.dao.impl;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import np.com.mshrestha.bookstore.dao.PersonDao;
import np.com.mshrestha.bookstore.model.Person;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PersonDaoImpl implements PersonDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void savePerson(Person person) {
		getSession().merge(person);

	}

	@SuppressWarnings("unchecked")
	public List<Person> listPersons() {

		return getSession().createCriteria(Person.class).list();
	}

	public Person getPerson(Integer id) {
		return (Person) getSession().get(Person.class, id);
	}

	public List<Person> getPersonByName(String name) {
		String query = "select * from user where name like '%"+name+"%'";
		return getSession().createSQLQuery(query).addEntity(Person.class).list();
	}

	public void deletePerson(Integer id) {

		Person person = getPerson(id);

		if (null != person) {
			getSession().delete(person);
		}

	}

	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public List<Person> viewAllPersons(int offset, int noOfRecords) {
		Query query1=getSession().createQuery("from Person");
		query1.setFirstResult((offset-1)*noOfRecords);
		query1.setMaxResults(noOfRecords);
		return (List<Person>) query1.list();
	}

	public int tableSize() {
		return getSession().createQuery("from Person").list().size();
	}
}
