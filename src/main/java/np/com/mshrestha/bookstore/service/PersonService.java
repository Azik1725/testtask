package np.com.mshrestha.bookstore.service;

import java.util.List;

import np.com.mshrestha.bookstore.model.Person;

public interface PersonService {

	/*
	 * CREATE and UPDATE 
	 */
	public void savePerson(Person person);

	/*
	 * READ
	 */
	public List<Person> listPersons();
	public Person getPerson(Integer id);

	/*
     *  Search
     */
	public List<Person> getPersonByName(String name);

	/*
	 * DELETE
	 */
	public void deletePerson(Integer id);
	public List<Person> viewAllPersons(int offset,int noOfRecords);
	public int tableSize();
}
