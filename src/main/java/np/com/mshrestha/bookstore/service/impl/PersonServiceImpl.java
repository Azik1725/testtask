package np.com.mshrestha.bookstore.service.impl;

import java.util.List;

import np.com.mshrestha.bookstore.dao.PersonDao;
import np.com.mshrestha.bookstore.model.Person;

import np.com.mshrestha.bookstore.service.PersonService;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDao personDao;

    @Transactional
    public void savePerson(Person person) {
        personDao.savePerson(person);
    }

    @Transactional(readOnly = true)
    public List<Person> listPersons() {
        return personDao.listPersons();
    }

    @Transactional(readOnly = true)
    public Person getPerson(Integer id) {
        return personDao.getPerson(id);
    }

    @Transactional
    public List<Person> getPersonByName(String name) {
        return personDao.getPersonByName(name);
    }

    @Transactional
    public void deletePerson(Integer id) {
        personDao.deletePerson(id);

    }
    @Transactional
    public List<Person> viewAllPersons(int offset,int noOfRecords){
        return personDao.viewAllPersons(offset,noOfRecords);
    }

    @Transactional
    public int tableSize(){
        return personDao.tableSize();
    }


}
