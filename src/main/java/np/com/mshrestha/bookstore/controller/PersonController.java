package np.com.mshrestha.bookstore.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import np.com.mshrestha.bookstore.model.Person;
import np.com.mshrestha.bookstore.service.PersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    private static  int pageCount = 10;

    @RequestMapping(value = {"/", "/listPersons"})
    public String listPersons(Map<String, Object> map) {

        map.put("person", new Person());
        map.put("personList", personService.listPersons());
        map.put("searchPerson",new Person());

        return "listPersons";
    }

    @RequestMapping(value = {"/","/listPersons"}, method = RequestMethod.GET)
    public String listOfPersonsOnPage(@RequestParam(value = "page", required = false) Integer reqPage,Map<String,Object> map)
    {

        if (reqPage == null)
            reqPage = 1;

        int maxPage = (this.personService.tableSize()%pageCount) == 0 ? (this.personService.tableSize()/pageCount) : (this.personService.tableSize()/pageCount) + 1;
        int startpage = reqPage - 5 > 0?reqPage - 5:1;
        int endpage =  maxPage;
        int nextPage = reqPage == maxPage ? reqPage - 1 : reqPage;

        map.put("personList", this.personService.viewAllPersons(reqPage, pageCount));
        map.put("startpage", startpage);
        map.put("endpage", endpage);
        map.put("person", new Person());
        map.put("page", nextPage);
        map.put("currentPage", reqPage);
        map.put("searchPerson",new Person());

        return "listPersons";
    }


    @RequestMapping("/get/{personId}")
    public String getPerson(@PathVariable Integer personId, Map<String, Object> map) {

        Person person = personService.getPerson(personId);

        map.put("person", person);

        return "personForm";
    }

    @RequestMapping(value="/search",method = RequestMethod.POST)
    public String getPersonByName(@RequestParam(value = "name") String personName, Map<String, Object> map)
    {

        List<Person> person = personService.getPersonByName(personName);
        map.put("person", new Person());
        map.put("personList",person);

        return "searchPersons";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String savePerson(@ModelAttribute("person") Person person,
                           BindingResult result) {
        person.setCreatedDate(new Timestamp(new Date().getTime()));
        personService.savePerson(person);

		/*
         * Note that there is no slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the current path
		 */
        return "redirect:listPersons";
    }

    @RequestMapping("/delete/{personId}")
    public String deletePerson(@PathVariable("personId") Integer id) {

        personService.deletePerson(id);

		/*
		 * redirects to the path relative to the current path
		 */
        // return "redirect:../listPersons";

		/*
		 * Note that there is the slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the project root path
		 */
        return "redirect:/listPersons";
    }
}
