<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:url var="actionUrl" value="save" />

<form:form id="personForm" commandName="person" method="post"
	action="${actionUrl}" class="pure-form pure-form-aligned">

	<fieldset>
		<legend></legend>

		<div class="pure-control-group">
			<label for="name">Name</label>
			<form:input name = "customerId" path="name" placeholder="Person Name" />
		</div>
		<div class="pure-control-group">
			<label for="age">Age</label>
			<form:input name="age" id = "age" path="age" placeholder="Age" />
		</div>
		<div class="pure-control-group">
			<form:checkbox path="isAdmin" label="Is Admin?"/>
		</div>
		<form:input path="id" type="hidden" />

	</fieldset>
</form:form>
