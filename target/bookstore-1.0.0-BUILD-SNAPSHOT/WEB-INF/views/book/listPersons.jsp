<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>List Of Persons</title>

    <link rel="stylesheet"
          href='<c:url value="/resources/css/pure-0.4.2.css"/>'>

    <link rel="stylesheet"
          href='<c:url value="/resources/css/font-awesome-4.0.3/css/font-awesome.css"/>'>

    <link rel="stylesheet"
          href='<c:url value="/resources/css/jquery-ui-1.10.4.custom.css"/>'>

    <style type="text/css">
        th {
            text-align: left
        }
    </style>

    <script type="text/javascript">

    </script>

</head>

<body>

<div style="width: 95%; margin: 0 auto;">
    <%--<c:url var="CountofOnePage" value="10"/>--%>
    <div id="personDialog" style="display: none;">
        <%@ include file="personForm.jsp" %>
    </div>

    <h1>List Of Persons</h1>

        <c:url var="searchUrl" value="/search"/>
        <form:form id="searchForm" commandName="person" method="post"
                   action="${searchUrl}" class="pure-form pure-form-aligned">
            <div class="pure-control-group">
                <label for="name">Name for Search</label>
                <form:input name = "customerId" path="name" placeholder="Person Name" />
                <form:button>Search</form:button>
            </div>

        </form:form>

        <button class="pure-button pure-button-primary" onclick="addPerson()">
        <i class="fa fa-plus"></i> Add Person
    </button>
    <br>

    <table class="pure-table pure-table-bordered pure-table-striped">
        <thead>
        <tr>
            <th width="4%">S.N</th>
            <th width="4%">PersonId</th>
            <th width="12%">Name</th>
            <th width="12%">Age</th>
            <th width="12%">isAdmin</th>
            <th width="12%">CreatedDate</th>
            <th width="12%"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${personList}" var="person" varStatus="loopCounter">
            <tr>
                <td><c:out value="${loopCounter.count}"/></td>
                <td><c:out value="${person.id}"/></td>
                <td><c:out value="${person.name}"/></td>
                <td><c:out value="${person.age}"/></td>
                <td><c:out value="${person.isAdmin}"/></td>
                <td><c:out value="${person.createdDate}"/></td>
                <td>
                    <nobr>
                        <button class="pure-button pure-button-primary"
                                onclick="editPerson(${person.id});">

                            <i class="fa fa-pencil"></i> Edit
                        </button>

                        <a class="pure-button pure-button-primary"
                           onclick="return confirm('Are you sure you want to delete this person?');"
                           href="delete/${person.id}"> <i class="fa fa-times"></i>Delete
                        </a>

                    </nobr>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
<br><br>
        <ul style="margin: 0; padding: 4px;">
        <li style=" display: inline;margin-right: 5px;padding: 3px; "><th>Go to page:</th></li>
        <li style=" display: inline;margin-right: 5px;;padding: 3px; "><c:forEach begin="${startpage}" end="${endpage}" var="p">
            <c:url var="pageUrl" value="/listPersons?page=${p}" />
            <c:if test="${p == currentPage}">
                <a href="${pageUrl}" class="pure-button pure-button-primary" style="color: #0005d1;">${p}</a>
            </c:if>
            <c:if test="${ p != currentPage}">
                <a href="${pageUrl}" class="pure-button pure-button-primary">${p}</a>
            </c:if>
        </c:forEach></li>
        <li style=" display: inline;margin-right: 5px;padding: 3px; "><c:url var="pageUrl" value="/listPersons?page=${page + 1}" />
        <a href="${pageUrl}" class="pure-button pure-button-primary">Next page</a></li>
        </ul>
</div>
    <!--  It is advised to put the <script> tags at the end of the document body so they don't block rendering of the page -->
    <script type="text/javascript"
            src='<c:url value="/resources/js/lib/jquery-1.10.2.js"/>'></script>
    <script type="text/javascript"
            src='<c:url value="/resources/js/lib/jquery-ui-1.10.4.custom.js"/>'></script>
    <script type="text/javascript"
            src='<c:url value="/resources/js/lib/jquery.ui.datepicker.js"/>'></script>
    <script type="text/javascript"
            src='<c:url value="/resources/js/js-for-listPersons.js"/>'></script>
    <script type="text/javascript" src="/resources/js/lib/pajinate-0.2/jquery.pajinate.js"></script>
</body>
</html>